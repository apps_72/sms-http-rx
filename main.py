#!/usr/bin/env python3.10
import pika
import os
import time
import json
import argparse
import sys
import re
import subprocess
import psycopg2
import mysql.connector
import jsontree
import datetime
import logging
import sentry_sdk
from flask import Flask
from flask import Flask, request, current_app, g, jsonify, request_finished, make_response, abort
from sentry_sdk.integrations.logging import LoggingIntegration
import tornado
import tornado.web
import tornado.options
from ipaddress import IPv4Address
import ipaddr, ipaddress
import tempfile
import uuid
import smtplib
import ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from urllib.parse import urlencode
from urllib.request import Request, urlopen
import requests
import memorize
from json import dumps
if os.environ.get('THREADS'):
    threads = int(os.environ['THREADS'])
else:
    threads = 0

app = Flask(__name__)
#------------------
#-----CONF PARAMS--
treads_count_started = 3
now = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())
formatted_command = "empty"
log_file_name = "empty"
version = os.getenv('APP_VERSION', 'unknown')
executor = os.uname()[1]

#-----------------
#-----VARS ENV----
HTTP_HOST_INCOMING = os.getenv('HTTP_HOST_INCOMING', 'incoming-demo.linux2be.com')
HTTP_HOST_OUTGOING = os.getenv('HTTP_HOST_OUTGOING', 'outgoing-demo.linux2be.com')
HTTP_URL_OUTGOING = os.getenv('HTTP_URL_OUTGOING', 'https://rx.example.com/api/sms/receive')
DB_TYPE = os.getenv('DB_TYPE', 'postgres') #postgres OR mysql
DB_CACHE_TIMEOUT      = int(os.getenv('DB_CACHE_TIMEOUT', 300))
DB_HOST      = os.getenv('DB_HOST', '127.0.0.1')
DB_PORT      = os.getenv('DB_PORT', '3306')
DB_USER      = os.getenv('DB_USER', 'convy')
DB_PASSWORD  = os.getenv('DB_PASSWORD', 'sdsdsdsds')
DB_NAME      = os.getenv('DB_NAME', 'convy')


SCRIPTS_STORE_DIR        = os.getenv('SCRIPTS_STORE_DIR', '/tmp')

SENTRY_URL               = os.getenv('SENTRY_URL', 'https://abc5140d3a9242b38dbbc6b0136bcf44:f28aa7bc642c4c34a4d253d12e2ldadf@sentry.example.com/2')
SENTRY_SERVER_NAME       = os.getenv('SENTRY_SERVER_NAME', executor)
ENV_DEBUG      = os.getenv('ENV_DEBUG', 'True')


logging.getLogger().addHandler(logging.StreamHandler())

def get_logger():
    logger = logging.getLogger("threading_example")
    logger.setLevel(logging.DEBUG)

    fh = logging.FileHandler("threading.log")
    fmt = '%(asctime)s - %(threadName)s - %(levelname)s - %(message)s'
    formatter = logging.Formatter(fmt)
    fh.setFormatter(formatter)

    logger.addHandler(fh)
    return logger


def send_post(url,r_content_type,headers,payload):
    if r_content_type  == "application/json":
        r = requests.post(url,data=json.dumps(payload),headers=headers)
    else:
        print("send_post with no json " + str(r_content_type))
        print('headers = ' + str(headers))
        r = requests.post(url,data=payload,headers=headers)
    if r.status_code == 200:
        print('Success! ' + str(r.reason))
        r_data = json.dumps({"r_code":r.status_code,"r_reason":str(r.reason),"r_content":str(r.content),"r_text":str(r.text),"r_headers":dict(r.headers) })
    else:
        print('Error. ' + str(r.reason))
        r_data = json.dumps({"r_code":r.status_code,"r_reason":str(r.reason),"r_content":str(r.content),"r_text":str(r.text),"r_headers":dict(r.headers) })
    
    return r_data

def send_get(url,r_content_type,r_data):
    get_args = r_data['args']
    headers = r_data['headers']
    print("Exec query headers")
    print(headers)
    print("Exec query get_args")
    print(get_args)
    r = requests.get(url,params=get_args,headers=headers)
    print("r.status_code = " +  str(r.status_code))
    if r.status_code == 200:
        print('Success! ' + str(r.reason))
        r_data = json.dumps({"r_code":r.status_code,"r_reason":str(r.reason),"r_content":str(r.content),"r_text":str(r.text),"r_headers":dict(r.headers) })
    else:
        print('Error. ' + str(r.reason))
        r_data = json.dumps({"r_code":r.status_code,"r_reason":str(r.reason),"r_content":str(r.content),"r_text":str(r.text),"r_headers":dict(r.headers) })
    return r_data

@memorize.memorize(timeout=DB_CACHE_TIMEOUT)
def db_data_get_mysql_ips():
    db_data = []
    try:
        con = mysql.connector.connect(
            host=DB_HOST,
            port=int(DB_PORT),
            user=DB_USER,
            password=DB_PASSWORD,
            ssl_disabled=True,
            db=DB_NAME)
        cur = con.cursor()
        cur.execute("select ip from sms_allowed_ips where active = 1 and deleted_at IS NULL")
        for row in cur.fetchall():
            db_data.append(row)
    except:
        logging.exception("Error. Select from mysql db " + str(DB_NAME) + "failed")
    try:
        con.close()
    except:
        pass
    return db_data


@memorize.memorize(timeout=DB_CACHE_TIMEOUT)
def db_data_get_pg_ips():
    db_data = []
    try:
        connection = psycopg2.connect(user=DB_USER, password=DB_PASSWORD,
                                      host=DB_HOST, port=DB_PORT, database=DB_NAME)
        cursor = connection.cursor()
        SQL = "select ip from sms_allowed_ips where active is True and deleted_at IS NULL;"
        print(SQL)
        cursor.execute(SQL)
        rows = cursor.fetchall()
        #print(rows)
        cursor.close()
        for row in rows:
            db_data.append(row)
        print(db_data)
        return db_data
    except (Exception, psycopg2.DatabaseError) as error:
        error_data = "error db_data_get_pg_ips"
        return error_data
    finally:
        if connection is not None:
            connection.close()
    



def save_request(uuid, request):

    req_data = {}

    req_data['uuid'] = uuid
    req_data['endpoint'] = request.endpoint
    req_data['method'] = request.method
    req_data['Content-Type'] = request.content_type
    req_data['headers'] = dict(request.headers)
    req_data['headers']['X-Auth'] = dict(request.headers)['Remote-Addr']
    req_data['headers']['Host'] = HTTP_HOST_OUTGOING
    req_data['headers']['http_host_origin'] = req_data['headers']['Host'].split(":", 1)[0]
    req_data['headers']['http_host_target'] = HTTP_HOST_OUTGOING
    req_data['remote_addr'] = req_data['headers']['Remote-Addr']
    req_data['headers'].pop('Cookie', None)
    if req_data['method'] == "POST" and req_data['Content-Type'] == "application/json":
        req_data['data'] = request.get_json(force = True)
        req_data['args'] = request.args
        print(f"{request.args=}")
    elif req_data['method'] == "GET":
        req_data['args'] = request.args
        print(f"{request.args=}")
    elif req_data['method'] == "POST" and req_data['Content-Type'] != "application/json":
        req_data['args'] = request.args
        req_data['data_plain'] = request.form     
    return req_data

def save_response(uuid, resp):
    resp_data = {}
    resp_data['uuid'] = uuid
    resp_data['status_code'] = resp.status_code
    resp_data['status'] = resp.status
    resp_data['headers'] = dict(resp.headers)
    resp_data['data'] = resp.response

    return resp_data


@app.before_request
def before_request():
    g.uuid = str(uuid.uuid4())
    #print(request.method, request.endpoint)

@app.before_request
def block_method():
    if DB_TYPE == "mysql":
       db_data = db_data_get_mysql_ips()
    else:
       db_data = db_data_get_pg_ips()
    print(db_data)
    ip_addr_v4_whitelist = []
    for row in db_data:
        ip_addr_v4_white = IPv4Address(int(row[0]))
        ip_addr_v4_whitelist.append(ip_addr_v4_white)
    #ip = request.environ.get('REMOTE_ADDR')
    ip = request.headers['Remote-Addr']
    m_http_host = request.headers['Host'].split(":", 1)
    m_http_host = m_http_host[0]
    print("---Debug equest.headers---")
    print(request.headers)
    print("---Debug equest.headers---")
    print("---ip_addr_v4_whitelist---")
    print(str(ip_addr_v4_whitelist))
    print("---ip_addr_v4_whitelist---")
    ###
    rule = request.url_rule

    if '/api/sms/healthcheck/front' in rule.rule:
        print('Healthcheck only')
    else:
        if ipaddress.ip_address(ip) in ip_addr_v4_whitelist:
            print("Accepted connection from " + ip)
        else:
            print("Forbidden connection from " + ip)
            abort(403, 'Forbidden')
        if m_http_host in HTTP_HOST_INCOMING:
            print("Request for host " + m_http_host + " accepted" + " from ip " + ip )
        else:
            print("Request for host " + m_http_host + " not allowed" + " from ip " + ip )
            print("Allowed hosts is = " + HTTP_HOST_INCOMING )
            abort(404)


@app.after_request
def after_request(resp):
    resp.headers.add('Access-Control-Allow-Origin', '*')
    resp.headers.add('Access-Control-Allow-Headers', 'Content-Type, X-Token, X-Auth')
    resp.headers.add('Access-Control-Allow-Methods', 'GET, POST')
    resp_data = save_response(g.uuid, resp)

    return resp

@app.route('/api/sms/healthcheck/front', methods=['GET', 'POST'])
def web_healthcheck():
    data = {'message': 'online', 'status': 'healhy'}
    return make_response(jsonify(data), 200)

@app.route('/', methods=['GET', 'POST'])

def main_route():
    print("-------------------------------" + " R_START " + "-------------------------------")

    req_data = save_request(g.uuid, request)
    resp = json.dumps(req_data, indent=4)
    m_http_host = req_data['headers']['Host'].split(":", 1)[0]
    r_data = json.loads(resp)
    print("HTTP_HOST_INCOMING = " + HTTP_HOST_INCOMING)
    print("HTTP_HOST_OUTGOING = " + HTTP_HOST_OUTGOING)
    print("HTTP_Content_Type = " + str(req_data['Content-Type']))

    if req_data['method'] == "POST" and req_data['Content-Type'] == "application/json":
        print("resend " + " Content-Type " + str(req_data['Content-Type']) + " to host " + HTTP_HOST_OUTGOING )
        r_send = send_post(HTTP_URL_OUTGOING,req_data['Content-Type'],r_data['headers'],r_data['data'])
    elif req_data['method'] == "POST" and req_data['Content-Type'] != "application/json":
        print("resend " + " Content-Type " + str(req_data['Content-Type']) + " to host " + HTTP_HOST_OUTGOING )
        r_send = send_post(HTTP_URL_OUTGOING,req_data['Content-Type'],r_data['headers'],r_data['data_plain'])
    elif req_data['method'] == "GET":
        print("resend " + " Content-Type " + str(req_data['Content-Type']) + " to host " + HTTP_HOST_OUTGOING )
        r_send = send_get(HTTP_URL_OUTGOING,req_data['Content-Type'],r_data)
    print("00001----00000----00001")
    r_data = json.loads(r_send)    
    #response = app.make_response(r_data['r_text'])
    #response.status_code = int(r_data['r_code'])
    response = app.response_class(
        status=int(r_data['r_code']),
        response=r_data['r_text']
    )
    response.headers
    print("-------------------------------" + " R_END " + "-------------------------------")
    return response 

    
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=ENV_DEBUG)
